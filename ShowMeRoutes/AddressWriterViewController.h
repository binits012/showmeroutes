//
//  AddressWriterViewController.h
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/6/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddressWriterViewControllerDelegate <NSObject>

- (void)getPlaceName:(NSString *)placeName andCoordinates:(NSString *)coordinates ofAddress1orAddress2:(BOOL)oneOrTwo;

@end
@interface AddressWriterViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource, UITableViewDelegate>{
    IBOutlet UILabel* myLabel;
    IBOutlet UITextField* myAddresField;
    IBOutlet UIButton* cancelButton;
    IBOutlet UITableView* tableView;
}
@property(nonatomic,assign)BOOL toOrFrom;
@property (nonatomic, unsafe_unretained) id<AddressWriterViewControllerDelegate>delegate;
-(IBAction)CancelButtonPressed:(id)sender;
@end
