//
//  ShowRoutesViewController.h
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/1/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerViewDelegate.h"
@class GADBannerView, GADRequest;
@interface ShowRoutesViewController : UITableViewController< GADBannerViewDelegate>

@property(nonatomic,strong)NSArray* informationArray;
@property(nonatomic,strong)NSString* startingAddress;
@property(nonatomic,strong)NSString* endingAddress;
@property (nonatomic, retain) GADBannerView *adBanner;

- (GADRequest *)createRequest;
@end
