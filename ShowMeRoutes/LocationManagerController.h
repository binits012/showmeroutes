//
//  LocationManagerController.h
//  TreeOfLife
//
//  Created by Iwa Labs on 7/19/11.
//  Copyright 2011 Loistava Interactive Oy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationManagerControllerDelegate
@optional

- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
 
@end

@interface LocationManagerController : NSObject <CLLocationManagerDelegate>

+ (float)getMyAccuracy;
+ (void)startUpdate ;
+ (void)stopUpdate;
+ (BOOL)hasLocation;
+ (BOOL)hasError;
+ (NSString *)getLat;
+ (NSString *)getLon;
+ (NSString*)getReverseGeoCodeLat;
+ (NSString*)getReverseGeoCodeLon;
+ (void)setDelegate:(id)_delegate;
+ (CLLocation *)getLocation;
 

@end


