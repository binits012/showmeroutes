//
//  ShowRoutesViewController.m
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/1/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import "ShowRoutesViewController.h"
#import "ShowRouteTableViewCell.h"
#import "GADBannerView.h"
#import "GADRequest.h"
#import "DetailRoutesViewController.h"
@interface ShowRoutesViewController ()

@end

@implementation ShowRoutesViewController
@synthesize informationArray;
@synthesize adBanner = adBanner_; 
@synthesize startingAddress,endingAddress;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
 
        
        self.adBanner = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
        self.adBanner.adUnitID = kAdMobId;
        self.adBanner.delegate = self;
        [self.adBanner setRootViewController:self];
       // [self.view addSubview:self.adBanner];
        self.tableView.tableHeaderView = self.adBanner;
        self.adBanner.center =
        CGPointMake(self.view.center.x, self.adBanner.center.y);
        [self.adBanner loadRequest:[self createRequest]];
        
        // Initiate a generic request to load it with an ad.
        [self.adBanner loadRequest:[GADRequest request]];
        self.tableView.bounces = NO;
        self.tableView.scrollEnabled = NO;
             UIView *footerView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        footerView1.backgroundColor = [UIColor clearColor];//[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255 alpha:1.0f];
        self.tableView.tableFooterView = footerView1;
        
   

   
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.title = @"Suggested Routes";
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger rowsCounter = [[informationArray valueForKey:@"legs"] count];
    return (rowsCounter >1)?rowsCounter:1;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView* titleView = [[UIView alloc ]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)];
    titleView.backgroundColor = [UIColor colorWithRed:72.0f/255.0f green:77.0f/255.0f blue:82.0f/255.0f alpha:0.8];
    
    UIView* buttonBarLine = [[UIView alloc]initWithFrame:CGRectMake(0, titleView.frame.size.height-2, titleView.bounds.size.width, 1)];
    buttonBarLine.backgroundColor = [UIColor blackColor];
    buttonBarLine.alpha = 0.15f;
    [titleView addSubview:buttonBarLine];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width, 40)];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = [NSString stringWithFormat:@"%@ - %@",startingAddress,endingAddress];
    [titleView addSubview:titleLabel];
    
    return titleView;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // customize the appearance of table view cells
	//
    
    static NSString *ShowRoutes = @"Cell";
    static NSString* LoadingCellIdentifier = @"LoadingCell";
    // add a placeholder cell while waiting on table data
    if ([[informationArray valueForKey:@"legs"] count] == 0 && indexPath.row == 0)
	{
        UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:LoadingCellIdentifier];
        if (cell == nil)
		{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:LoadingCellIdentifier] ;
            cell.detailTextLabel.textAlignment = NSTextAlignmentCenter;
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        
		cell.detailTextLabel.text = @"Loading…";
		
		return cell;
    }
    
    ShowRouteTableViewCell *routeCell = (ShowRouteTableViewCell *)[tableView1 dequeueReusableCellWithIdentifier:ShowRoutes];
    
    if (routeCell == nil)
    {
        routeCell = [[ShowRouteTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ShowRoutes ];
        routeCell.selectionStyle = UITableViewCellSelectionStyleGray;
        routeCell.accessoryType= UITableViewCellAccessoryDisclosureIndicator;
       
        UIView *myView = [[UIView alloc] initWithFrame:routeCell.frame];
        if(indexPath.row %2 == 0){
            myView.backgroundColor = [UIColor whiteColor];
            routeCell.backgroundView = myView;
 
        }else{
            myView.backgroundColor = [UIColor colorWithRed:240.0f/255.0f green:231.0f/255.0f blue:231.0f/255.0f alpha:1.0f];
            routeCell.backgroundView = myView;
 
        }
    }
    
   [routeCell setData:[informationArray objectAtIndex:indexPath.row] ];
    
    return routeCell;
 


        
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self performSegueWithIdentifier:@"DetailRoutes" sender:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    /*
     When a row is selected, the segue creates the detail view controller as the destination.
     Set the detail view controller's detail item to the item associated with the selected row.
     */
    if ([[segue identifier] isEqualToString:@"DetailRoutes"]) {
        
        NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
        DetailRoutesViewController *detailViewController = [segue destinationViewController];
        NSLog(@"selected row %d",selectedRowIndex.row);
        
        detailViewController.infoArray = [informationArray objectAtIndex:selectedRowIndex.row];
      
        
    }
}

 
#pragma mark GADRequest generation

// Here we're creating a simple GADRequest and whitelisting the application
// for test ads. You should request test ads during development to avoid
// generating invalid impressions and clicks.
- (GADRequest *)createRequest {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as
    // well as any devices you want to receive test ads.
    request.testDevices =
    [NSArray arrayWithObjects:nil, nil];
    return request;
}

#pragma mark GADBannerViewDelegate impl

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"Received ad successfully");
}

- (void)adView:(GADBannerView *)view
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
}



@end
