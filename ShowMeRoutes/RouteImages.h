//
//  RouteImages.h
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/8/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteImages : UIView{
    
}

-(void)createImages:(NSUInteger)numberOfImages andArray:(NSArray*)array;
@end
