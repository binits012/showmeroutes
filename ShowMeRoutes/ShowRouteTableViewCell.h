//
//  ShowRouteTableViewCell.h
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/6/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowRouteTableViewCell : UITableViewCell{
    NSArray* myInfoArray;
}
-(void)setData:(NSMutableArray*)infoArray;
@end
