//
//  ShowRouteTableViewCell.m
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/6/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import "ShowRouteTableViewCell.h"
#import "RouteImages.h"
@interface ShowRouteTableViewCell(){
    UILabel* staringTime;
    UILabel* endingTime;
    UILabel* walkingLabel;
    UILabel* durationLabel;
    UILabel* arrivalLabel;
   
    RouteImages* routeImages;
     
}
@end
@implementation ShowRouteTableViewCell
 
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UIView* routeFrame = self.contentView;
        
        ///////////////////////// Label Setting ///////////////////////////////////
        staringTime = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 30, 25)];
        staringTime.textColor=[UIColor darkTextColor];
        staringTime.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
        staringTime.backgroundColor = [UIColor clearColor];
        staringTime.textAlignment = NSTextAlignmentLeft;
        [staringTime setNumberOfLines:2];
        staringTime.textColor = [UIColor colorWithRed:7.0f/255.0f green:149.0f/255.0f blue:31.0f/255.0f alpha:1.0f];
        [staringTime setLineBreakMode:NSLineBreakByClipping];
        [routeFrame addSubview:staringTime];
        
        
        endingTime = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, 30, 25)];
        endingTime.textColor=[UIColor darkTextColor];
        endingTime.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
        endingTime.backgroundColor = [UIColor clearColor];
        endingTime.textColor = [UIColor colorWithRed:27.0/255.0f green:37.0f/255.0f blue:174.0f/255.0f alpha:1.0f];
        endingTime.textAlignment = NSTextAlignmentLeft;
        [endingTime setNumberOfLines:2];
        [endingTime setLineBreakMode:NSLineBreakByClipping];
        [routeFrame addSubview:endingTime];
        
        
        walkingLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 5, 105, 15)];
        walkingLabel.textColor=[UIColor darkTextColor];
        walkingLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:10];
        walkingLabel.backgroundColor = [UIColor clearColor];
        walkingLabel.textColor = [UIColor darkTextColor];
        walkingLabel.textAlignment = NSTextAlignmentLeft;
        [walkingLabel setLineBreakMode:NSLineBreakByClipping];
        [routeFrame addSubview:walkingLabel];
        
        durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 20, 105, 15)];
        durationLabel.textColor=[UIColor darkTextColor];
        durationLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:10];
        durationLabel.backgroundColor = [UIColor clearColor];
        durationLabel.textColor = [UIColor darkTextColor];
        durationLabel.textAlignment = NSTextAlignmentLeft;
        [durationLabel setLineBreakMode:NSLineBreakByClipping];
        [routeFrame addSubview:durationLabel];
        
        arrivalLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 35, 105, 15)];
        arrivalLabel.textColor=[UIColor darkTextColor];
        arrivalLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:10];
        arrivalLabel.backgroundColor = [UIColor clearColor];
        arrivalLabel.textColor = [UIColor darkTextColor];
        arrivalLabel.textAlignment = NSTextAlignmentLeft;
        [arrivalLabel setLineBreakMode:NSLineBreakByClipping];
        [routeFrame addSubview:arrivalLabel];
        
        routeImages = [[RouteImages alloc]initWithFrame:CGRectMake(45, 5, 160, 50)];
        routeImages.backgroundColor = [UIColor clearColor];
        [routeFrame addSubview:routeImages];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setData:(NSMutableArray *)infoArray{
 
    
    
   
    NSUInteger counter = [[[[[[infoArray objectAtIndex:0] valueForKey:@"legs"] valueForKey:@"locs"] objectAtIndex:0] valueForKey:@"arrTime"] count];
    NSString* startTime = [[[[[[infoArray objectAtIndex:0] valueForKey:@"legs"]valueForKey:@"locs"] objectAtIndex:0] valueForKey:@"depTime"] objectAtIndex:0] ;
    NSString* startingHour = [startTime substringWithRange:NSMakeRange(8, 2)];
    NSString* startingMinute = [startTime substringFromIndex:10];
    staringTime.text = [NSString stringWithFormat:@"Start\n%@:%@",startingHour,startingMinute];
    
    
    NSString* endTime = [[[[[[infoArray objectAtIndex:0]valueForKey:@"legs"] valueForKey:@"locs"] objectAtIndex:0] valueForKey:@"arrTime"] objectAtIndex:counter-1];
    NSString* endingHour = [endTime substringWithRange:NSMakeRange(8, 2)];
    NSString* endingMinute = [endTime substringFromIndex:10];
    endingTime.text = [NSString stringWithFormat:@"Get In\n%@:%@",endingHour,endingMinute];
   
    
    
    NSUInteger locsCount = [[[[infoArray objectAtIndex:0 ]  valueForKey:@"legs"] valueForKey:@"locs"]count];
    NSUInteger locsCount1 = [[[[[[infoArray objectAtIndex:0 ]  valueForKey:@"legs"] valueForKey:@"locs"]objectAtIndex:locsCount-1] valueForKey:@"arrTime"] count];
    
    NSString* tempArrivalTime =  [[[[[[infoArray objectAtIndex:0 ]  valueForKey:@"legs"] valueForKey:@"locs"]objectAtIndex:locsCount-1] valueForKey:@"arrTime"] objectAtIndex:locsCount1-1] ;
 
    
    NSString* tempArrivalHour = [tempArrivalTime substringWithRange:NSMakeRange(8, 2)];
    NSString* tempArrivalMinutes = [tempArrivalTime substringFromIndex:10];
    arrivalLabel.text = [NSString stringWithFormat:@"Arrival: %@:%@",tempArrivalHour,tempArrivalMinutes];
    
    NSUInteger durationCounter = [[[[infoArray valueForKey:@"legs"] objectAtIndex:0]valueForKey:@"duration"] count];
    NSUInteger tempTotalDuration = 0;
    for(NSUInteger i = 0; i< durationCounter; i++){
        tempTotalDuration += [[[[[infoArray valueForKey:@"legs"] objectAtIndex:0]valueForKey:@"duration"] objectAtIndex:i] integerValue];
    }
    
    durationLabel.text = [NSString stringWithFormat:@"Duration: %dmins",tempTotalDuration/60];
    
    //*********************total Walking Time *******************///
    
   // NSLog(@" %@ %d", [infoArray   valueForKey:@"legs"] ,[[[[infoArray objectAtIndex:0 ]  valueForKey:@"legs"] valueForKey:@"type"]count]  );
    
    NSUInteger walkCounter =  [[[[infoArray objectAtIndex:0 ]  valueForKey:@"legs"] valueForKey:@"type"]count] ;
    float tempTotalWalk = 0;
    for (NSUInteger i = 0; i < walkCounter; i++) {
        if([[[[[infoArray objectAtIndex:0 ]  valueForKey:@"legs"] valueForKey:@"type"]objectAtIndex:i] isEqualToString:@"walk"]){
           // NSLog(@"%d %d",i,[[[[[infoArray objectAtIndex:0 ]  valueForKey:@"legs"] valueForKey:@"length"]objectAtIndex:i] integerValue]);
            tempTotalWalk += [[[[[infoArray objectAtIndex:0 ]  valueForKey:@"legs"] valueForKey:@"length"]objectAtIndex:i]  floatValue];
            
        }
        
    }
  
    walkingLabel.text = [NSString stringWithFormat:@"Walking: %.02fkm",tempTotalWalk/1000];

    [routeImages createImages:walkCounter andArray:[[infoArray objectAtIndex:0 ]  valueForKey:@"legs"] ];
        

        
  
    
}
-(void)prepareForReuse{
    [super prepareForReuse];
    routeImages = nil;
}
@end
