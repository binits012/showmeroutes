//
//  ViewController.m
//  ShowMeRoutes
//
//  Created by binit shrestha on 7/31/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "PKRevealController.h"
#import "GADBannerView.h"
#import "GADRequest.h"
#import "AddressWriterViewController.h" 
#import "LocationManagerController.h"
#import "ShowRoutesViewController.h"

@interface ViewController ()<AddressWriterViewControllerDelegate>{

    
    NSString* address1Name;
    NSString* address2Name;
    
    
    NSString* address1Coordinates;
    NSString* address2Coordinates;
    
    AddressWriterViewController* addressWriterVC ;
     
    BOOL stopUpdateCurrentLocation;
     
}

@end

@implementation ViewController
@synthesize adBanner = adBanner_; 
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    myMap.delegate = self;
    addressHolder1.delegate = self;
    addressHolder2.delegate = self;
    hourHolder.delegate = self;
    minutesHolder.delegate = self;
    dateHolder.delegate = self;
    
    addressHolder1.backgroundColor = [UIColor clearColor];
    addressHolder1.textColor = [UIColor blackColor];
    addressHolder1.font = [UIFont systemFontOfSize:14.0f];
    addressHolder1.borderStyle = UITextBorderStyleNone;
    addressHolder1.clearButtonMode = UITextFieldViewModeWhileEditing;
    addressHolder1.returnKeyType = UIReturnKeyNext;
    addressHolder1.textAlignment = NSTextAlignmentCenter;
    addressHolder1.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    addressHolder1.tag = 1;
    addressHolder1.placeholder = @"From:";
    addressHolder1.autocapitalizationType = UITextAutocorrectionTypeNo;
    addressHolder1.autocapitalizationType = UITextAutocapitalizationTypeNone;
    addressHolder1.layer.cornerRadius = 0.0f;
    addressHolder1.layer.masksToBounds = YES;
    addressHolder1.layer.borderColor = [[UIColor blackColor]CGColor];
    addressHolder1.layer.borderWidth = 1.0f;
    addressHolder1.textColor = [UIColor colorWithRed:0 green:0 blue:255 alpha:1.0f];

    addressHolder2.backgroundColor = [UIColor clearColor];
    addressHolder2.textColor = [UIColor blackColor];
    addressHolder2.font = [UIFont systemFontOfSize:14.0f];
    addressHolder2.borderStyle = UITextBorderStyleNone;
    addressHolder2.clearButtonMode = UITextFieldViewModeWhileEditing;
    addressHolder2.returnKeyType = UIReturnKeyNext;
    addressHolder2.textAlignment = UITextAlignmentCenter;
    addressHolder2.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    addressHolder2.tag = 2;
    addressHolder2.autocapitalizationType = UITextAutocorrectionTypeNo;
    addressHolder2.placeholder = @"To:";
    addressHolder2.autocapitalizationType = UITextAutocapitalizationTypeNone;
    addressHolder2.layer.cornerRadius = 0.0f;
    addressHolder2.layer.masksToBounds = YES;
    addressHolder2.layer.borderColor = [[UIColor blackColor]CGColor];
    addressHolder2.layer.borderWidth = 1.0f;
    addressHolder2.textColor = [UIColor colorWithRed:255 green:0 blue:0 alpha:1.0f];
    
    hourHolder.backgroundColor = [UIColor clearColor];
    hourHolder.textColor = [UIColor blackColor];
    hourHolder.font = [UIFont systemFontOfSize:12.0f];
    hourHolder.borderStyle = UITextBorderStyleNone;
    hourHolder.returnKeyType = UIReturnKeyNext;
    hourHolder.keyboardType = UIKeyboardTypeNamePhonePad;
    hourHolder.textAlignment = NSTextAlignmentCenter;
    hourHolder.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    hourHolder.tag = 3;
    hourHolder.autocapitalizationType = UITextAutocorrectionTypeNo;
    hourHolder.autocapitalizationType = UITextAutocapitalizationTypeNone;
    hourHolder.layer.cornerRadius = 0.0f;
    hourHolder.layer.masksToBounds = YES;
    hourHolder.layer.borderColor = [[UIColor blackColor]CGColor];
    hourHolder.layer.borderWidth = 1.0f;
    hourHolder.textColor = [UIColor colorWithRed:0 green:0 blue:255 alpha:1.0f];
    
    minutesHolder.backgroundColor = [UIColor clearColor];
    minutesHolder.textColor = [UIColor blackColor];
    minutesHolder.font = [UIFont systemFontOfSize:12.0f];
    minutesHolder.borderStyle = UITextBorderStyleNone;
    minutesHolder.returnKeyType = UIReturnKeyNext;
    minutesHolder.keyboardType = UIKeyboardTypeNamePhonePad;
    minutesHolder.textAlignment = NSTextAlignmentCenter;
    minutesHolder.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    minutesHolder.tag = 4;
    minutesHolder.autocapitalizationType = UITextAutocorrectionTypeNo;
    minutesHolder.autocapitalizationType = UITextAutocapitalizationTypeNone;
    minutesHolder.layer.cornerRadius = 0.0f;
    minutesHolder.layer.masksToBounds = YES;
    minutesHolder.layer.borderColor = [[UIColor blackColor]CGColor];
    minutesHolder.layer.borderWidth = 1.0f;
    minutesHolder.textColor = [UIColor colorWithRed:0 green:0 blue:255 alpha:1.0f];
    
    dateHolder.backgroundColor = [UIColor clearColor];
    dateHolder.textColor = [UIColor blackColor];
    dateHolder.font = [UIFont systemFontOfSize:12.0f];
    dateHolder.borderStyle = UITextBorderStyleNone;
    dateHolder.returnKeyType = UIReturnKeyDone;
    dateHolder.keyboardType = UIKeyboardTypeNamePhonePad;
    dateHolder.textAlignment = NSTextAlignmentCenter;
    dateHolder.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    dateHolder.tag = 5;
    dateHolder.autocapitalizationType = UITextAutocorrectionTypeNo;
    dateHolder.autocapitalizationType = UITextAutocapitalizationTypeNone;
    dateHolder.layer.cornerRadius = 0.0f;
    dateHolder.layer.masksToBounds = YES;
    dateHolder.layer.borderColor = [[UIColor blackColor]CGColor];
    dateHolder.layer.borderWidth = 1.0f;
    dateHolder.textColor = [UIColor colorWithRed:0 green:0 blue:255 alpha:1.0f];
    dateHolder.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
    
    
    if (self.navigationController.revealController.type & PKRevealControllerTypeLeft)
    {
        
        self.navigationItem.leftBarButtonItem = [self menuButton];
        
    }
    
    if (self.navigationController.revealController.type & PKRevealControllerTypeRight)
    {
        
        self.navigationItem.rightBarButtonItem = [self rightButton];
        
    }
   
    
    
    self.adBanner = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
    
    
    // Note: Edit SampleConstants.h to provide a definition for kSampleAdUnitID
    // before compiling.
    self.adBanner.adUnitID = kAdMobId;
    self.adBanner.delegate = self;
    [self.adBanner setRootViewController:self];
    [self.view addSubview:self.adBanner];
    self.adBanner.center =
    CGPointMake(self.view.center.x, self.adBanner.center.y);
    [self.adBanner loadRequest:[self createRequest]];
    
    // Initiate a generic request to load it with an ad.
    [self.adBanner loadRequest:[GADRequest request]];
    
    
    // On iOS 6 ADBannerView introduces a new initializer, use it when available.
    if ([ADBannerView instancesRespondToSelector:@selector(initWithAdType:)]) {
        _bannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
    } else {
        _bannerView = [[ADBannerView alloc] init];
    }
    _bannerView.delegate = self;
    [iadView addSubview:  _bannerView];
     
    searchButton.enabled = NO;
    stopUpdateCurrentLocation = NO;
   
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self displayCurrentDateNTime];
   
}
-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.001;
    span.longitudeDelta = 0.001;
    region.span = span;
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude =  myMap.userLocation.coordinate.latitude;
    zoomLocation.longitude = myMap.userLocation.coordinate.longitude;
    NSLog(@"%lf",myMap.userLocation.coordinate.longitude);
    region.center = zoomLocation;
    [myMap setRegion:region animated:YES];
    [myMap regionThatFits:region];
    [myMap setZoomEnabled:YES];
    [myMap setScrollEnabled:YES];
    
    if(stopUpdateCurrentLocation == NO){
        NSString* lat = [NSString stringWithFormat:@"%lf",myMap.userLocation.coordinate.latitude];
        NSString* lon = [NSString stringWithFormat:@"%lf",myMap.userLocation.coordinate.longitude];
        [self getGeoReveresedAddress:lat andLong:lon];
    }
   
    
 
     
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 
-(void)address2Coordinates{
    NSString* address1TempLat = [addressHolder2.text stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kGeoCoding,address1TempLat];
    NSLog(@"%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/plain"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"App.net Global Stream: %@", [[JSON valueForKey:@"coords"] objectAtIndex:0]);
        address2Coordinates = [[JSON valueForKey:@"coords"] objectAtIndex:0];
        
        
    } failure:^(NSURLRequest *request, NSURLResponse *response, NSError *error, id JSON) {
        NSLog(@"failure %@", [error description]);
        address2Coordinates = [[JSON valueForKey:@"coords"] objectAtIndex:0];
        
    }];
    [operation start];
}
-(void)address1latitudeCoordinates{
    
    NSString* address1TempLat = [addressHolder1.text stringByReplacingOccurrencesOfString:@"ä" withString:@"a"];
    NSString* address = [address1TempLat stringByReplacingOccurrencesOfString:@"ö" withString:@"o"];
    NSString* address1 = [address stringByReplacingOccurrencesOfString:@"å" withString:@"a"];
    
    NSString* path = [NSString stringWithFormat:@"?request=geocode&user=binits&pass=ilovenone1312&format=json&key=%@",address1 ];
    
    NSLog(@"path %@",path);
    AfhttpClientBase* httpClient =   [AfhttpClientBase sharedManager];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"get"
                                                            path:path
                                                      parameters:nil];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/plain"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        address1Coordinates = [[JSON valueForKey:@"coords"] objectAtIndex:0];

        
        
    } failure:^(NSURLRequest *request, NSURLResponse *response, NSError *error, id JSON) {
        NSLog(@"failure %@", [error description]);
        address1Coordinates = [[JSON valueForKey:@"coords"] objectAtIndex:0];
    }];
    [operation start];
    
  
   
   
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"showRoutes"]){
        NSString* path = [NSString stringWithFormat:@"?request=route&user=binits&pass=ilovenone1312&format=json&from=%@&to=%@&date=%@&time=%@%@&timetype=depature&show=5&detail=full",address1Coordinates,address2Coordinates,dateHolder.text, hourHolder.text,minutesHolder.text];
        
        NSLog(@"path %@",path);
        AfhttpClientBase* httpClient =   [AfhttpClientBase sharedManager];
        
        NSMutableURLRequest *request = [httpClient requestWithMethod:@"get"
                                                                path:path
                                                          parameters:nil];
         ShowRoutesViewController* showRoutesViewController   = [segue destinationViewController];
        showRoutesViewController.startingAddress = addressHolder1.text;
        showRoutesViewController.endingAddress = addressHolder2.text;
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            
          //  NSLog(@"Response: %@", JSON);
           
            
             showRoutesViewController.informationArray = JSON ;
            
            [showRoutesViewController.tableView reloadData];
            
            
        } failure:^(NSURLRequest *request, NSURLResponse *response, NSError *error, id JSON) {
            NSLog(@"failure %@", [error description]);
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[error description] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }];

        [operation start];
       
         
    }
}
 
-(void)searchRoutes:(id)sender{
 
}
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{

//    NSLog(@"updateUserLocation");
//    NSLog(@"%f %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
     
 
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if(textField.tag == 3 || textField.tag == 4){
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS_ONLY] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return (([string isEqualToString:filtered])&&(newLength <= CHARACTER_LIMIT));
    }
    else
        return YES;
}

 
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField.tag == 1 || textField.tag == 2){
        [addressHolder1 resignFirstResponder];
        [addressHolder2 resignFirstResponder];
          [LocationManagerController startUpdate];
        UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone"
                                                      bundle:nil];
        addressWriterVC = [sb instantiateViewControllerWithIdentifier:@"addressWriterViewController"];
        addressWriterVC.delegate = self;
        if(textField.tag == 1){
            addressWriterVC.toOrFrom = YES;
        }else{
            addressWriterVC.toOrFrom = NO;
        }
        [self presentViewController:addressWriterVC animated:YES completion:nil];
        
    }
   
}
-(void)textFieldDidEndEditing:(UITextField *)textField{

    if(textField.tag == 2){
        if([textField.text length]==0){
            searchButton.enabled = NO;
        }else{
            searchButton.enabled = YES;
        }
    }
    if (textField.tag == 3) {
        if([hourHolder.text integerValue]>23){
            [self showErrorAlert:1];
            hourHolder.text = @"00";
        }
        if([hourHolder.text integerValue] == 0){
            hourHolder.text = @"00";
        }
    }
    if(textField.tag == 4){
        if([minutesHolder.text integerValue]>59){
            [self showErrorAlert:2];
           
        }
        if([minutesHolder.text integerValue] == 0){
            minutesHolder.text = @"00";
        }
    }
   

   
}
-(void)showErrorAlert:(NSUInteger)number{
    UIAlertView* alertView;
    if(number == 1){
        alertView = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Hours cannot be morethan 23" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }else if(number == 2){
        alertView = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Minutes cannot be morethan 60" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];

    }
    [alertView show];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    switch (textField.tag) {
       
        case 3:
            if([hourHolder.text integerValue]>23){
                [self showErrorAlert:1];
                 hourHolder.text = @"";
                [hourHolder becomeFirstResponder];
            }else{
                [minutesHolder becomeFirstResponder];
            }
 
            break;
        case 4:
            if([minutesHolder.text integerValue]>59){
                [self showErrorAlert:2];
                minutesHolder.text = @"";
                [minutesHolder becomeFirstResponder];
            }else{
                [dateHolder becomeFirstResponder];
            }
            
            break;
        case 5:
            [dateHolder resignFirstResponder];
            break;
        default:
            break;
    }
    return YES;
}
 
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}


#pragma mark -
#pragma private methods
-(void)displayCurrentDateNTime{

    // get the current date
    NSDate *date = [NSDate date];
    // format it
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"YYYYMMdd"];
    // convert it to a string
    NSString *dateString = [dateFormat stringFromDate:date];
    dateHolder.text = dateString;
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc]init];
    [dateFormat1 setDateFormat:@"HH:mm"];
    dateString = [dateFormat1 stringFromDate:date];
    hourHolder.text = [NSString stringWithFormat:@"%@",[dateString substringToIndex:2] ];
    minutesHolder.text = [NSString stringWithFormat:@"%@",[dateString substringFromIndex:3]];
}


// Menu Button
- (UIBarButtonItem *)menuButton {
    UIImage *leaveATipImage = [UIImage imageNamed:@"reveal_menu_icon_portrait"];
    // UIImage *leaveATipPressed = [UIImage imageNamed:@"explore"];
    UIButton *leaveATipButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leaveATipButton addTarget:self action:@selector(showLeftView:) forControlEvents:UIControlEventTouchUpInside];
    
    leaveATipButton.showsTouchWhenHighlighted = YES;
    [leaveATipButton setBackgroundImage:leaveATipImage forState:UIControlStateNormal];
    // [leaveATipButton setBackgroundImage:leaveATipPressed forState:UIControlStateHighlighted];
    
    // const CGFloat BarButtonOffset = 0.0f;
    
    [leaveATipButton setFrame:CGRectMake(5, 10, leaveATipImage.size.width, leaveATipImage.size.height)];
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leaveATipImage.size.width+10, leaveATipImage.size.height+20)];
    
    [containerView addSubview:leaveATipButton];
    
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:containerView];
    
    return item;
}

- (UIBarButtonItem *)rightButton {
    UIImage *leaveATipImage = [UIImage imageNamed:@"reveal_menu_icon_portrait"];
    // UIImage *leaveATipPressed = [UIImage imageNamed:@"explore"];
    UIButton *leaveATipButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leaveATipButton addTarget:self action:@selector(showRightView:) forControlEvents:UIControlEventTouchUpInside];
    
    leaveATipButton.showsTouchWhenHighlighted = YES;
    [leaveATipButton setBackgroundImage:leaveATipImage forState:UIControlStateNormal];
    
    // const CGFloat BarButtonOffset = 0.0f;
    
    [leaveATipButton setFrame:CGRectMake(5, 10, leaveATipImage.size.width, leaveATipImage.size.height)];
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leaveATipImage.size.width+10, leaveATipImage.size.height+20)];
    
    [containerView addSubview:leaveATipButton];
    
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:containerView];
    
    return item;
}

- (void)showLeftView:(id)sender
{
    if (self.navigationController.revealController.focusedController == self.navigationController.revealController.leftViewController)
    {
        [self.navigationController.revealController showViewController:self.navigationController.revealController.frontViewController];
    }
    else
    {
        [self.navigationController.revealController showViewController:self.navigationController.revealController.leftViewController];
    }
}

- (void)showRightView:(UIButton*)sender
{
    
    if (self.navigationController.revealController.focusedController == self.navigationController.revealController.rightViewController)
    {
        [self.navigationController.revealController showViewController:self.navigationController.revealController.frontViewController];
    }
    else
    {
        [self.navigationController.revealController showViewController:self.navigationController.revealController.rightViewController];
        
    }
}



-(void)getGeoReveresedAddress:(NSString*)lat andLong:(NSString*)lon{

    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&sensor=true",lat,lon];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
     
        NSString *status = [JSON valueForKey:@"status"];
        if([status isEqualToString:@"OK"]){
            NSArray *firstResultAddress = [[[JSON objectForKey:@"results"] objectAtIndex:0] objectForKey:@"address_components"];
            NSLog(@"location %@",[[firstResultAddress valueForKey:@"long_name"] objectAtIndex:1]);
            address1Name =   [NSString stringWithFormat:@"%@",[[firstResultAddress valueForKey:@"long_name"] objectAtIndex:1]];
            addressHolder1.text = address1Name;
            self.navigationItem.title = [[[[[JSON objectForKey:@"results"] objectAtIndex:1] objectForKey:@"address_components"] valueForKey:@"long_name"] objectAtIndex:1];
            [self address1latitudeCoordinates];
            
        }
        
    } failure:nil];
    [operation start];
}



#pragma mark GADRequest generation

// Here we're creating a simple GADRequest and whitelisting the application
// for test ads. You should request test ads during development to avoid
// generating invalid impressions and clicks.
- (GADRequest *)createRequest {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as
    // well as any devices you want to receive test ads.
    request.testDevices =
    [NSArray arrayWithObjects:nil, nil];
    return request;
}

#pragma mark GADBannerViewDelegate impl

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"Received ad successfully");
}

- (void)adView:(GADBannerView *)view
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
}



- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    iadView = _bannerView;
     
}
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner
               willLeaveApplication:(BOOL)willLeave
{
    return YES;
}
- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    
}
- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"%@",[error description]);
}



#pragma mark - addressWriterViewController Delegated Method

-(void)getPlaceName:(NSString *)placeName andCoordinates:(NSString *)coordinates ofAddress1orAddress2:(BOOL)oneOrTwo{
    if(oneOrTwo){
        stopUpdateCurrentLocation = YES;
        addressHolder1.text = placeName;
        address1Coordinates = coordinates;
        [addressHolder1 setNeedsDisplay];
        
    }else{
        addressHolder2.text = placeName;
        address2Coordinates = coordinates;
        [addressHolder2 setNeedsDisplay];
        searchButton.enabled = YES;
    }
}
@end
