//
//  AppDelegate.h
//  ShowMeRoutes
//
//  Created by binit shrestha on 7/31/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PKRevealController;
@class RightViewController;
@class LeftViewController;
@class ViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic,readwrite) PKRevealController *viewController1;
@property (strong, nonatomic) ViewController* viewController;
@property (strong, nonatomic) RightViewController* rightViewController;
@property (strong,nonatomic) LeftViewController* leftViewController;
@end
