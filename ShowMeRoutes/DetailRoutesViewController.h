//
//  DetailRoutesViewController.h
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/8/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerViewDelegate.h"
@class GADBannerView, GADRequest;
@interface DetailRoutesViewController : UITableViewController< GADBannerViewDelegate>
@property(nonatomic,strong)NSArray* infoArray;
@property (nonatomic, retain) GADBannerView *adBanner;
- (GADRequest *)createRequest;
@end
