//
//  AfhttpClientBase.h
//  BreakitApp
//
//  Created by binit shrestha on 2/19/13.
//  Copyright (c) 2013 Eighty-Five Media. All rights reserved.
//

#import "AFHTTPClient.h"
 
@interface AfhttpClientBase : AFHTTPClient

+(id)sharedManager;
@end
