//
//  RouteImages.m
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/8/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import "RouteImages.h"

@implementation RouteImages

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)createImages:(NSUInteger)numberOfImages andArray:(NSArray*)array{
 
    CGFloat horizontalOffset = 0;
    NSUInteger width = 0;
    
    if(numberOfImages >=5){
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 23*numberOfImages, self.frame.size.height);
        width = 23;
    }else{
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 32*numberOfImages, self.frame.size.height);
        width = 32;
    }
    
    
    
    for (NSUInteger i = 0 ; i < numberOfImages ; i++)
    {
        
        
        UIImageView* myIcons = [self imageAtIndex:i andWidth:self.frame.size.width/numberOfImages];
        myIcons.frame = CGRectMake(horizontalOffset, 0, myIcons.frame.size.width, myIcons.frame.size.height);
        
        [self addSubview:myIcons];
        
        UILabel* myLabel = [self labelAtIndex:i andWidth:self.frame.size.width/numberOfImages];
        myLabel.frame = CGRectMake(horizontalOffset, myIcons.frame.size.height, myIcons.frame.size.width, 15);
        [self addSubview:myLabel];
        horizontalOffset += width;
       // NSLog(@"my array %d %@",i,[array objectAtIndex:i]);
        
        if([[[array objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"walk"]){
            myIcons.image  = [UIImage imageNamed:@"walk@2x"];
            myLabel.text = [NSString stringWithFormat:@"%.02f",[[[array objectAtIndex:i] valueForKey:@"length"] floatValue]/1000 ] ;
        }
        else if([[[array objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"12"]){
            myIcons.image = [UIImage imageNamed:@"train@2x"];
            NSString* train =  [[array objectAtIndex:i] valueForKey:@"code"];
            
            NSString *trainName = [[train componentsSeparatedByCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]  ] componentsJoinedByString:@""];
          //  NSLog(@"pureNumbers%@",trainName);
            myLabel.text = [NSString stringWithFormat:@"%@",trainName];
            
        }
        else if([[[array objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"6"]){
            myIcons.image = [UIImage imageNamed:@"metro@2x"];
            myLabel.text = @"M";
        }
        else if([[[array objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"2"]){
            myIcons.image = [UIImage imageNamed:@"tram@2x"];
            NSString* tram =  [[array objectAtIndex:i] valueForKey:@"code"];
            
            myLabel.text = [self nameOfTheVehicle:tram];
        }
        else if([[[array objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"7"]){
            myIcons.image = [UIImage imageNamed:@"ferry@2x"];
            myLabel.text = @"F";
        }else {
            myIcons.image = [UIImage imageNamed:@"bus@2x"];
            NSString* bus =  [[array objectAtIndex:i] valueForKey:@"code"];
            
            myLabel.text = [self nameOfTheVehicle:bus];
        }
        
    }
    
    
    
}

-(NSString*)nameOfTheVehicle:(NSString*)string{
    
  NSString*  tempString = [string substringWithRange:NSMakeRange(1, [string length]-2)];
    
    NSString* character = [tempString substringWithRange:NSMakeRange([tempString length]-2, 1)];
    
    string = [NSString stringWithFormat:@"%d%@",[tempString integerValue],character];
    
     
    return string;
}

-(UIImageView*)imageAtIndex:(NSUInteger)index andWidth:(CGFloat)width{
    UIImageView* myImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, width, width)];
    // myImage.backgroundColor = [UIColor blueColor];
    return myImage;
}
-(UILabel*)labelAtIndex:(NSUInteger)index andWidth:(CGFloat)width{
    UILabel* myLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, width)];
    myLabel.backgroundColor = [UIColor clearColor];
    myLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:8.0f];
    myLabel.textAlignment = NSTextAlignmentCenter;
    return myLabel;
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
