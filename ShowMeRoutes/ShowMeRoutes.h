//
//  ShowMeRoutes.h
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/1/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#ifndef ShowMeRoutes_ShowMeRoutes_h
#define ShowMeRoutes_ShowMeRoutes_h
#define NUMBERS_ONLY @"1234567890"
#define CHARACTER_LIMIT 2
#define kAdMobId @"a1519570c59abbe"
#define kGeoCoding @"http://api.reittiopas.fi/hsl/prod/?request=geocode&user=binits&pass=ilovenone1312&format=json&key="
#define kRouteBetweenTwoPoints @"http://api.reittiopas.fi/hsl/prod/?request=route&user=binits&pass=ilovenone1312&format=json&from="
#define kReverseGeoCoding @"http://api.reittiopas.fi/hsl/prod/?request=reverse_geocode&user=binits&pass=ilovenone1312&format=json&coordinate="
#define kBaseURL @"http://api.reittiopas.fi/hsl/prod/"

#endif
