//
//  main.m
//  ShowMeRoutes
//
//  Created by binit shrestha on 7/31/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
