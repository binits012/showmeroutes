//
//  ViewController.h
//  ShowMeRoutes
//
//  Created by binit shrestha on 7/31/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <iAd/iAd.h>
#import "GADBannerViewDelegate.h"
@class GADBannerView, GADRequest;
@interface ViewController : UIViewController<MKMapViewDelegate,UITextFieldDelegate, GADBannerViewDelegate,ADBannerViewDelegate>{
    IBOutlet MKMapView* myMap;
    IBOutlet UITextField* addressHolder1;
    IBOutlet UITextField* addressHolder2;
    
    IBOutlet UIButton* searchButton;
    IBOutlet UITextField* hourHolder;
    IBOutlet UITextField* minutesHolder;
    IBOutlet UITextField* dateHolder;
    ADBannerView *_bannerView;
    IBOutlet UIView* iadView;
     
}
@property (nonatomic, retain) GADBannerView *adBanner;

- (GADRequest *)createRequest;
-(IBAction)searchRoutes:(id)sender;
@end
