//
//  AddressWriterViewController.m
//  ShowMeRoutes
//
//  Created by binit shrestha on 8/6/13.
//  Copyright (c) 2013 Eighty-Five Media Oy. All rights reserved.
//

#import "AddressWriterViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "LocationManagerController.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
@interface AddressWriterViewController (){
    NSString* currentLocation;
    BOOL typed;
    NSArray* jsonArray;
    NSArray* coordArray;
    NSString* currentLocationCoordinates;
    
}

@end

@implementation AddressWriterViewController
@synthesize toOrFrom;
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    myAddresField.delegate = self;
    myAddresField.backgroundColor = [UIColor clearColor];
    myAddresField.font = [UIFont systemFontOfSize:14.0f];
    myAddresField.borderStyle = UITextBorderStyleNone;
    myAddresField.clearButtonMode = UITextFieldViewModeWhileEditing;
    myAddresField.returnKeyType = UIReturnKeyDone;
    myAddresField.textAlignment = NSTextAlignmentLeft;
    myAddresField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    myAddresField.autocapitalizationType = UITextAutocorrectionTypeNo;
    myAddresField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    myAddresField.layer.cornerRadius = 0.0f;
    myAddresField.layer.masksToBounds = YES;
    myAddresField.layer.borderColor = [[UIColor clearColor]CGColor];
    myAddresField.layer.borderWidth = 1.0f;
    myAddresField.textColor = [UIColor blackColor];
    
   
    
  
    UIView *footerView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    footerView1.backgroundColor = [UIColor clearColor];//[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255 alpha:1.0f];
    tableView.tableFooterView = footerView1;
    
   
   
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(toOrFrom){
        myLabel.text = @"From";
    }
    else{
        myLabel.text = @"To";
    }
     typed = NO;
    [myAddresField becomeFirstResponder];
    myLabel.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.7f];
    myLabel.textColor = [UIColor colorWithRed:0 green:0 blue:255 alpha:1.0f];
    myLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    myLabel.textAlignment = NSTextAlignmentCenter;
    
    cancelButton.layer.borderColor = [[UIColor clearColor]CGColor];
    cancelButton.layer.cornerRadius = 0.0f;
    cancelButton.layer.borderWidth = 1.0f;
    cancelButton.layer.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:1.0f].CGColor;
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [self getCurrentAddress];
    [LocationManagerController stopUpdate];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)CancelButtonPressed:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)address1latitudeCoordinates{
 
    NSString* path = [NSString stringWithFormat:@"?request=geocode&user=binits&pass=ilovenone1312&format=json&key=%@",currentLocation];
    
    
    AfhttpClientBase* httpClient =   [AfhttpClientBase sharedManager];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"get"
                                                            path:path
                                                      parameters:nil];
    
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSLog(@"Response: %@", [[JSON valueForKey:@"coords"] objectAtIndex:0]);
        currentLocationCoordinates = [[JSON valueForKey:@"coords"] objectAtIndex:0];
 
    } failure:nil];
    [operation start];

    
}
-(void)getCurrentAddress{
    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&sensor=true",[LocationManagerController getLat],[LocationManagerController getLon]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSString *status = [JSON valueForKey:@"status"];
        if([status isEqualToString:@"OK"]){
            NSArray *firstResultAddress = [[[JSON objectForKey:@"results"] objectAtIndex:0] objectForKey:@"address_components"];
            NSLog(@"location %@",[[firstResultAddress valueForKey:@"long_name"] objectAtIndex:1]);
            currentLocation =   [NSString stringWithFormat:@"%@",[[firstResultAddress valueForKey:@"long_name"] objectAtIndex:1]];
            self.navigationItem.title = [[[[[JSON objectForKey:@"results"] objectAtIndex:1] objectForKey:@"address_components"] valueForKey:@"long_name"] objectAtIndex:1];
            [self address1latitudeCoordinates];
            NSLog(@"currentLocation %@",currentLocation);
            [tableView reloadData];
           
        }
        
    } failure:nil];
    [operation start];

}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(typed){
        return [[jsonArray valueForKey:@"name"] count];
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
        UIView* titleView = [[UIView alloc ]initWithFrame:CGRectMake(0, 0, 280, 20)];
        titleView.backgroundColor = [UIColor colorWithRed:72.0f/255.0f green:77.0f/255.0f blue:82.0f/255.0f alpha:0.8];
        
        UIView* buttonBarLine = [[UIView alloc]initWithFrame:CGRectMake(0, titleView.frame.size.height-2, titleView.bounds.size.width, 1)];
        buttonBarLine.backgroundColor = [UIColor blackColor];
        buttonBarLine.alpha = 0.15f;
        [titleView addSubview:buttonBarLine];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 280, 20)];
        titleLabel.font = [UIFont boldSystemFontOfSize:14];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        if (typed) {
            titleLabel.text = @"Some Suggestions";
        }else{
            titleLabel.text = @"My location";
        }
         
        [titleView addSubview:titleLabel];
        
        return titleView;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}
- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // customize the appearance of table view cells
	//
	 
    static NSString *PlaceholderCellIdentifier = @"PlaceholderCell";
    
    // add a placeholder cell while waiting on table data
    
	
	 
	 
        UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:PlaceholderCellIdentifier];
        if (cell == nil)
		{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:PlaceholderCellIdentifier] ;
            cell.detailTextLabel.textAlignment = NSTextAlignmentCenter;
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.detailTextLabel.backgroundColor = [UIColor clearColor];
            cell.textLabel.backgroundColor = [UIColor clearColor];
            
        }
        if(indexPath.row %2 == 0){
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }else{
            cell.contentView.backgroundColor = [UIColor colorWithRed:240.0f/255.0f green:231.0f/255.0f blue:231.0f/255.0f alpha:1.0f];
        }
        if(typed){
            cell.textLabel.text =  [[jsonArray valueForKey:@"name"] objectAtIndex:indexPath.row];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"City: %@ Location Type: %@",[[jsonArray valueForKey:@"city"] objectAtIndex:indexPath.row],[[jsonArray valueForKey:@"locType"] objectAtIndex:indexPath.row]];
        }else{
            cell.textLabel.text = @"Current Location";
            cell.detailTextLabel.text =  currentLocation;
        }
	 

    return cell;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(typed){
    
        [delegate getPlaceName: [[jsonArray valueForKey:@"name"] objectAtIndex:indexPath.row] andCoordinates: [[jsonArray valueForKey:@"coords"] objectAtIndex:indexPath.row] ofAddress1orAddress2:toOrFrom];
        
    }else{
        [delegate getPlaceName:currentLocation andCoordinates:currentLocationCoordinates ofAddress1orAddress2:toOrFrom];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - uitextfielddelegates
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
     
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
      
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if(newLength >= 4){
            typed = YES;
            NSLog(@"check this %@",textField.text);
            [self getStopNames];
        }else if(newLength < 4){
            typed = NO;
            [tableView reloadData];
        }
    if([string isEqual:@"\n"]){
        [myAddresField resignFirstResponder];
        
    }
        return YES;
    
         
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
        
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
         
    
}

-(void)getStopNames{

    NSString* path = [NSString stringWithFormat:@"?request=geocode&user=binits&pass=ilovenone1312&format=json&key=%@",myAddresField.text];
    
    
   AfhttpClientBase* httpClient =   [AfhttpClientBase sharedManager];
   
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"get"
                                                            path:path
                                                      parameters:nil];
 
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
         NSLog(@"check stops: %@", JSON);
        jsonArray = JSON;
        [tableView reloadData];
      
        
    } failure:nil];
    [operation start];

}

@end
