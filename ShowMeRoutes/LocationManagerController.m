 

#import "LocationManagerController.h"
#import <CoreLocation/CoreLocation.h>

static CLLocationManager *locationManager;
static NSString *lat;
static NSString *lon;
static NSString* reverseGeoCodingLat;
static NSString* reverseGeoCodingLon;
static BOOL hasLocation = false;
static UIAlertView *alert = NULL;
static LocationManagerController *loc;
static BOOL hasError = false;
static id delegate;
static float myAccuracy;
 
 
@implementation LocationManagerController

- (void)initLoc
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    
    
}

+ (void) setDelegate:(id) _delegate
{
    delegate = _delegate;
}

+ (void)startUpdate 
{
     
    hasLocation = false;
    hasError = false;  
    if (!loc) {
        loc = [LocationManagerController alloc];
        [loc initLoc];
    }
    [locationManager startUpdatingLocation];
   
}

+ (void)stopUpdate
{
    [locationManager stopUpdatingLocation];
}

+ (BOOL)hasLocation
{
    return hasLocation;
}

+ (BOOL)hasError
{
    return hasError;
}

+ (float)getMyAccuracy
{
    return myAccuracy;
}

+ (NSString*)getLat
{
    return lat;
}

+ (NSString*)getLon
{
    return lon;
}


+ (NSString*)getReverseGeoCodeLat{
    return reverseGeoCodingLat;
}
+ (NSString*)getReverseGeoCodeLon{
    return reverseGeoCodingLon;
}
+ (CLLocation *)getLocation
{
    return [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
}

- (void)locationManager: (CLLocationManager *)manager
	   didFailWithError: (NSError *)error
{
	switch([error code])
	{
		case kCLErrorNetwork: // general, network-related error
			alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Could not detemine location please make sure location services are enabled." delegate:NULL cancelButtonTitle:@"OK" otherButtonTitles:NULL];
			[alert show];
            [locationManager stopUpdatingLocation];
            [manager stopUpdatingLocation];
            hasError = true;
            break;
            
		case kCLErrorDenied:
			alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Could not determine location please make sure you have allowed ShowMeRoutes to use your location." delegate:NULL cancelButtonTitle:@"OK" otherButtonTitles:NULL];
			[alert show];
            [locationManager stopUpdatingLocation];
            [manager stopUpdatingLocation];
            hasError = true;
            break;
	}
    
    if ([delegate respondsToSelector:@selector(locationError:)]) {
        [delegate locationError:error];
    }
}

 

 #if __IPHONE_6_0 
// code to compile for 6.0 or later
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    
    // If it's a relatively recent event, turn off updates to save power
    CLLocation* location = [locations lastObject];
    
    CLLocation* oldlocation = [locations objectAtIndex:0];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 5.0) {
        // If the event is recent, do something with it.
        NSLog(@"123  latitude %+.6f, longitude %+.6f\n %f  %f",
              location.coordinate.latitude,
              location.coordinate.longitude,location.horizontalAccuracy, location.verticalAccuracy);
        
        if (location.coordinate.latitude != oldlocation.coordinate.latitude ) {
            
            lat = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
            lon = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
            
        }
        else{
            lat = [NSString stringWithFormat:@"%f", oldlocation.coordinate.latitude];
            lon = [NSString stringWithFormat:@"%f", oldlocation.coordinate.longitude];
        
        }
        
    if(location.horizontalAccuracy <= 100){
        [locationManager stopUpdatingLocation];
    }
        
    }
    
}
#endif
// code to compile for pre-4.0

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"abc  latitude %+.6f, longitude %+.6f\n %f  %f",
          newLocation.coordinate.latitude,
          newLocation.coordinate.longitude,newLocation.horizontalAccuracy, newLocation.verticalAccuracy);
    
   
             if ((newLocation.horizontalAccuracy <= 100 && newLocation.verticalAccuracy <= 50)  ) {
            
            [locationManager stopUpdatingLocation];
            lat = [NSString stringWithFormat:@"%f", newLocation.coordinate.latitude];
            lon = [NSString stringWithFormat:@"%f", newLocation.coordinate.longitude];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"latitudeLongitudeSendToPostView" object:nil];
        }
        
 
    
}

 
 
@end
