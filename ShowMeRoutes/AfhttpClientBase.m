//
//  AfhttpClientBase.m
//  BreakitApp
//
//  Created by binit shrestha on 2/19/13.
//  Copyright (c) 2013 Eighty-Five Media. All rights reserved.
//

#import "AfhttpClientBase.h"
#import "AFJSONRequestOperation.h"
@implementation AfhttpClientBase
+(id)sharedManager{

    static AfhttpClientBase *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[AfhttpClientBase alloc] initWithBaseURL:
                            [NSURL URLWithString:kBaseURL]];
    });
    
    return __sharedInstance;

}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        //custom settings
 
        [self setDefaultHeader:@"Accept" value:@"application/json"];
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    }
    
    return self;
}
@end
